fenics (2:0.9.0.1) unstable; urgency=medium

  * fenicsx loads dolfinx 0.9.0 with ufl 2024.2
  * Add Francesco Ballarin to uploaders

 -- Francesco Ballarin <francesco.ballarin@unicatt.it>  Sun, 03 Nov 2024 13:57:48 +0000

fenics (2:0.8.0.1) unstable; urgency=medium

  * update versions for the v0.8 release (including ufl 2024.1.0)
  * debian/tests: test all supported python versions
  * fenicsx Suggests: python3-adios4dolfinx
    (note Adios2 is available on 64-bit arches only)
  * Standards-Version: 4.7.0

 -- Drew Parsons <dparsons@debian.org>  Thu, 09 May 2024 13:33:36 +0200

fenics (2:0.7.0.1) unstable; urgency=medium

  * fenicsx loads dolfinx 0.7 with ufl 2023.2
  * fenicsx Suggests: fenicsx-performance-tests
    ("advertising" the benchmark tests)

 -- Drew Parsons <dparsons@debian.org>  Wed, 01 Nov 2023 11:50:51 +0100

fenics (2:0.6.0.3) unstable; urgency=medium

  * no epoch version with Suggests: python3-dolfinx-mpc (0.6 not 1:0.6)

 -- Drew Parsons <dparsons@debian.org>  Mon, 26 Jun 2023 03:24:20 +0200

fenics (2:0.6.0.2) unstable; urgency=medium

  * fenicsx Suggests: python3-dolfinx-mpc
    - debian/tests tests import of python3-dolfinx-mpc
      (not mips64el, mipsel)

 -- Drew Parsons <dparsons@debian.org>  Mon, 26 Jun 2023 03:16:38 +0200

fenics (2:0.6.0.1) unstable; urgency=medium

  * fenicsx loads dolfinx 0.6 with UFL 2023
  * fenics loads legacy dolfin patched to use ufl_legacy (UFL 2022)
    - debian/tests tests import of ufl_legacy
  * remove redundant debian/README.source

 -- Drew Parsons <dparsons@debian.org>  Mon, 26 Jun 2023 02:48:44 +0200

fenics (2:0.5.0.1) unstable; urgency=medium

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure URI in Homepage field.

  [ Drew Parsons ]
  * present FEniCS-X (dolfinx) 0.5
    - uses UFL 2022.2.0
  * Standards-Version: 4.6.2

 -- Drew Parsons <dparsons@debian.org>  Wed, 18 Jan 2023 23:51:11 +0100

fenics (2:0.4.1.1) unstable; urgency=medium

  * reorganise automated versioning to read FENICS-X version from
    source package version, set ufl version to 2022.1.0, and
    fenics legacy version to 2019.2.0
  * new source package epoch bump to manage version jump from FEniCS-X
    2019.2.0 back down to 0.4.1.
  * use ~ with package versions to facilitate backporting
  * dolfinx now uses basix instead of FIAT
  * update list of mshr-supported arches. mshr now builds on all arches
    except armel mips mipsel.
  * Standards-Version: 4.6.1

 -- Drew Parsons <dparsons@debian.org>  Sun, 26 Jun 2022 18:15:47 +0200

fenics (1:2019.2.0.5) unstable; urgency=medium

  * Depend on common packages, don't enforce "real" builds.

 -- Drew Parsons <dparsons@debian.org>  Sun, 04 Oct 2020 11:45:48 +0800

fenics (1:2019.2.0.4) unstable; urgency=medium

  * fenicx: Depends: libdolfinx-real-dev, python3-dolfinx-real
    (to ensure full installation installing Recommends)

 -- Drew Parsons <dparsons@debian.org>  Fri, 07 Aug 2020 20:36:19 +0800

fenics (1:2019.2.0.3) unstable; urgency=medium

  * fenics Depends: python3-dolfin-real
    (Depend alongside python3-dolfin to ensure full installation)
  * debhelper compatibility level 13

 -- Drew Parsons <dparsons@debian.org>  Tue, 04 Aug 2020 01:45:34 +0800

fenics (1:2019.2.0.2) unstable; urgency=medium

  * mark fenicsx as Architecture: all
    (arch-specificity in fenics was due to availability of mshr)
  * update arch list for mshr (now available on powerpc)
  * Suggests: not Recommends: python3-mshr where not available

 -- Drew Parsons <dparsons@debian.org>  Wed, 22 Apr 2020 11:45:52 +0800

fenics (1:2019.2.0.1) unstable; urgency=medium

  * add test-fenicsx to debian/tests
  * update debian/copyright to modern format

 -- Drew Parsons <dparsons@debian.org>  Tue, 21 Apr 2020 22:53:02 +0800

fenics (1:2019.2.0.0) experimental; urgency=medium

  * link to new upstream release 2019.2.0
  * provide fenicsx for the next-generation implementation FEniCX-X,
    using python3-dolfinx, python3-ffcx.
  * Standards-Version: 4.5.0

 -- Drew Parsons <dparsons@debian.org>  Sat, 01 Feb 2020 10:14:13 +0800

fenics (1:2019.1.0.3) unstable; urgency=medium

  * set OMPI_MCA_btl_base_warn_component_unused=0 in debian/tests to
    avoid warning about missing OpenFabrics (openib) importing dolfin
  * Standards-Version: 4.4.1

 -- Drew Parsons <dparsons@debian.org>  Sun, 27 Oct 2019 09:49:30 +0800

fenics (1:2019.1.0.2) unstable; urgency=medium

  * Standards-Version: 4.4.0

 -- Drew Parsons <dparsons@debian.org>  Mon, 05 Aug 2019 20:41:04 +0800

fenics (1:2019.1.0.1exp1) experimental; urgency=medium

  * New upstream release.
  * Standards-Version: 4.3.0
  * debhelper compatibility level 12
  * Depends: python3-mshr for armhf i386 hppa (now building ok)

 -- Drew Parsons <dparsons@debian.org>  Tue, 21 May 2019 21:17:11 +0800

fenics (1:2018.1.0.6) unstable; urgency=medium

  * mark the fenics package as Multi-Arch: same

 -- Drew Parsons <dparsons@debian.org>  Thu, 16 Aug 2018 22:28:11 +0800

fenics (1:2018.1.0.5) unstable; urgency=medium

  * add debian/tests (autopkgtest) to check that FEniCS components
    import successfully in python3

 -- Drew Parsons <dparsons@debian.org>  Thu, 16 Aug 2018 20:46:58 +0800

fenics (1:2018.1.0.4) unstable; urgency=medium

  * Recommends: python3-mshr on 32 bit arches
    [armel armhf i386 mips mipsel hppa m68k powerpc x32]

 -- Drew Parsons <dparsons@debian.org>  Sat, 11 Aug 2018 10:09:47 +0800

fenics (1:2018.1.0.3) unstable; urgency=medium

  * use correct url for Vcs-Git in debian/control
  * exclude 32 bit arches from mshr dependency (they don't have enough
    memory available to build mshr):
    Depends: python3-mshr [!armel !armhf !i386 !mips !mipsel
      !hppa !m68k !powerpc !x32]
  * "Architecture: any" instead of "all", to accommodate architecture
    restrictions on mshr.

 -- Drew Parsons <dparsons@debian.org>  Fri, 10 Aug 2018 20:37:33 +0800

fenics (1:2018.1.0.2) unstable; urgency=medium

  * Depends: python3-mshr. mshr is now available.
    Closes: #851183.
  * Standards-Version: 4.2.0

 -- Drew Parsons <dparsons@debian.org>  Sat, 04 Aug 2018 14:29:05 +0800

fenics (1:2018.1.0.1) unstable; urgency=medium

  * New upstream version
    - no longer depends on python-instant
    - now supports Python 3 only
  * Standards-Version: 4.1.5

 -- Drew Parsons <dparsons@debian.org>  Sat, 28 Jul 2018 20:51:58 +0800

fenics (1:2017.2.0.1) unstable; urgency=medium

  [ Johannes Ring ]
  * Remove python-scitools from Recommends.

  [ Drew Parsons ]
  * New upstream version 2017.2.0.
  * Standards-Version: 4.1.3
  * debhelper compatibility level 11
  * update VCS tags to https://salsa.debian.org
  * Priority: optional not extra

 -- Drew Parsons <dparsons@debian.org>  Fri, 23 Feb 2018 00:52:55 +0800

fenics (1:2017.1.0.1) unstable; urgency=medium

  * Team upload.

  [ Johannes Ring ]
  * debian/control:
    - Update VCS fields after the move to Git (closes: #854624).

  [ Drew Parsons ]
  * New upstream version 2017.1.0
  * Standards-Version: 4.1.0
  * debhelper compatibility level 10
  * Identify in the long description that fenics is a dummy
    metapackage depending on all FEniCS components.
  * Use minimal dh in debian/rules instead of cdbs.
  * Suggests: python-mshr (>= 2016.2.0)
    - mshr (python-mshr) may be built from
      https://anonscm.debian.org/git/debian-science/packages/fenics/mshr.git/
      (reported in README.Debian)

 -- Drew Parsons <dparsons@debian.org>  Tue, 12 Sep 2017 21:25:16 +0800

fenics (1:2016.2.0.1) unstable; urgency=medium

  * debian/control:
    - Update Depends field to DOLFIN 2016.2.0, FFC 2016.2.0, FIAT
      2016.2.0, Instant 2016.2.0 and UFL 2016.2.0.
    - Add python-dijitso to Depends.
    - Remove Christophe Prud'homme from Uploaders (closes: #835014).

 -- Johannes Ring <johannr@simula.no>  Thu, 05 Jan 2017 14:14:32 +0100

fenics (1:2016.1.0.1) UNRELEASED; urgency=medium

  * debian/control:
    - Update Depends field to DOLFIN 2016.1.0, FFC 2016.1.0, FIAT
      2016.1.0, Instant 2016.1.0 and UFL 2016.1.0.
    - Bump Standards-Version to 3.9.8 (no changes needed).

 -- Johannes Ring <johannr@simula.no>  Mon, 27 Jun 2016 19:16:22 +0200

fenics (1:1.5.0.1) unstable; urgency=medium

  * debian/control:
    - Update Depends field to DOLFIN 1.5.0, FFC 1.5.0, FIAT 1.5.0,
      Instant 1.5.0 and UFL 1.5.0.
    - Bump Standards-Version to 3.9.6 (no changes needed).

 -- Johannes Ring <johannr@simula.no>  Tue, 24 Mar 2015 10:11:46 +0100

fenics (1:1.4.0.1) unstable; urgency=medium

  * debian/control:
    - Update Depends field to DOLFIN 1.4.0, FFC 1.4.0, FIAT 1.4.0,
      Instant 1.4.0 and UFL 1.4.0.
    - Remove ufc, python-ufc and python-ferari from Depends field
      (closes: #755728, LP: #1353018).
    - Remove libsyfi1.0-dev, python-syfi, syfi-doc and sfc from Suggest.

 -- Johannes Ring <johannr@simula.no>  Fri, 29 Aug 2014 12:48:12 +0200

fenics (1:1.3.0.1) unstable; urgency=low

  * debian/control:
    - Update Depends field to DOLFIN 1.3.0, FFC 1.3.0, FIAT 1.3.0,
      Instant 1.3.0, UFC 2.3.0 and UFL 1.3.0.
    - Bump Standards-Version to 3.9.5.
  * Remove unused lintian override no-upstream-changelog.

 -- Johannes Ring <johannr@simula.no>  Thu, 20 Feb 2014 10:26:41 +0100

fenics (1:1.2.0.1) unstable; urgency=low

  * debian/control:
    - Update Depends field to DOLFIN 1.2.0, FFC 1.2.0, Instant 1.2.0,
      UFC 2.2.0 and UFL 1.2.0.
    - Move SyFi releated packages to Suggests.
    - Remove DM-Upload-Allowed field.
    - Bump Standards-Version to 3.9.4.
    - Use canonical URIs for Vcs-* fields.
  * Use a Debian native version number.

 -- Johannes Ring <johannr@simula.no>  Tue, 26 Mar 2013 11:38:25 +0100

fenics (1:1.1.0-1) UNRELEASED; urgency=low

  * debian/control:
    - Update Depends field to DOLFIN 1.1.0, FFC 1.1.0, FIAT 1.1, Instant
      1.1.0, UFC 2.1.0 and UFL 1.1.0.
    - Remove python-viper from Depends field.
    - Bump Standards-Version to 3.9.3 (no changes needed).
    - Update description field.

 -- Johannes Ring <johannr@simula.no>  Fri, 11 Jan 2013 09:02:24 +0100

fenics (1:1.0.0-1) unstable; urgency=low

  * debian/control: Update Depends field for latest FEniCS releases.

 -- Johannes Ring <johannr@simula.no>  Thu, 08 Dec 2011 11:16:06 +0100

fenics (1:1.0-rc2-1) unstable; urgency=low

  * debian/control: Update Depends field to DOLFIN 1.0-rc2 and FFC
    1.0-rc1 and UFC 2.0.4.

 -- Johannes Ring <johannr@simula.no>  Tue, 29 Nov 2011 13:08:49 +0100

fenics (1:1.0-rc1-1) unstable; urgency=low

  * debian/control:
    - Update Homepage field.
    - Update Depends field to DOLFIN 1.0-rc1 and UFL 1.0-rc1.

 -- Johannes Ring <johannr@simula.no>  Tue, 22 Nov 2011 18:31:16 +0100

fenics (1:1.0-beta2-1) unstable; urgency=low

  * debian/control:
    - Update Depends field to DOLFIN 1.0-beta2, FFC 1.0-beta2, UFC 2.0.3
      and UFL 1.0-beta3.
    - Add SyFi back as a dependency.
    - Add python-scitools to Recommends.

 -- Johannes Ring <johannr@simula.no>  Thu, 27 Oct 2011 14:44:46 +0200

fenics (1:1.0-beta-1) unstable; urgency=low

  * Update Depends field in debian/control for latest FEniCS releases.
  * Add lintian overrides:
    - fenics-source: native-package-with-dash-version
    - fenics: no-upstream-changelog
    - fenics: empty-binary-package

 -- Johannes Ring <johannr@simula.no>  Wed, 24 Aug 2011 08:00:01 +0200

fenics (11.05.2) unstable; urgency=low

  * debian/control: Remove syfi* from Depends since they are currently
    uninstallable together with the other FEniCS packages
    (closes: #629564).

 -- Johannes Ring <johannr@simula.no>  Sat, 11 Jun 2011 11:02:15 +0200

fenics (11.05-1) unstable; urgency=low

  * debian/control:
    - Bump Standards-Version to 3.9.2 (no changes needed).
    - Make package architecture all (closes: #614979, #604527).
    - Update Depends field to DOLFIN 0.9.11, FFC 0.9.10, Instant 0.9.10,
      UFC 2.0.1 and UFL 0.9.1.

 -- Johannes Ring <johannr@simula.no>  Wed, 18 May 2011 17:34:23 +0200

fenics (11.02-1) unstable; urgency=low

  * Update Depends field in debian/control.
  * Update Homepage field in debian/control (closes: #603477).

 -- Johannes Ring <johannr@simula.no>  Thu, 24 Feb 2011 12:45:38 +0100

fenics (10.09-1) unstable; urgency=low

  * debian/control: Update to DOLFIN 0.9.9, FFC 0.9.4, UFC 1.4.2, and
    UFL 0.5.4 in Depends field for binary package fenics.

 -- Johannes Ring <johannr@simula.no>  Tue, 14 Sep 2010 09:31:05 +0200

fenics (10.06-2) unstable; urgency=low

  * debian/control:
    - Disable fenics binary package on architectures where DOLFIN is
      uninstallable (Closes: #593731).
    - Bump Standards-Version to 3.9.1 (no changes needed).

 -- Johannes Ring <johannr@simula.no>  Wed, 25 Aug 2010 08:45:42 +0200

fenics (10.06-1) unstable; urgency=low

  * debian/control:
    - Update Depends to latest versions of FEniCS packages.
    - Minor fix in Vcs-Browser field.
    - Bump Standards-Version to 3.9.0 (no changes needed).
  * Switch to dpkg-source 3.0 (native) format.

 -- Johannes Ring <johannr@simula.no>  Sat, 03 Jul 2010 21:03:23 +0200

fenics (10.02-1) unstable; urgency=low

  * debian/control:
    - Bump Standards-Version to 3.8.4 (no changes needed).
    - Update Depends to latest versions of FEniCS packages.
    - Replace dolfin-dev and python-pydolfin0 with libdolfin0-dev and
      python-dolfin, respectively, in Depends field.
  * Package moved from pkg-scicomp to Debian Science.

 -- Johannes Ring <johannr@simula.no>  Wed, 28 Apr 2010 11:41:35 +0200

fenics (9.12.1) unstable; urgency=low

  * Initial release (Closes: #503265)

 -- Johannes Ring <johannr@simula.no>  Thu, 14 Jan 2010 13:06:21 +0100
