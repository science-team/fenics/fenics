Source: fenics
Section: math
Priority: optional
Maintainer: Debian Science Team <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Johannes Ring <johannr@simula.no>,
 Drew Parsons <dparsons@debian.org>,
 Francesco Ballarin <francesco.ballarin@unicatt.it>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.7.0
Homepage: https://fenicsproject.org
Vcs-Git: https://salsa.debian.org/science-team/fenics/fenics.git
Vcs-Browser: https://salsa.debian.org/science-team/fenics/fenics

Package: fenics
Architecture: any
Multi-Arch: same
Depends: libdolfin-dev (>= ${fenics:Upstream-Version}),
 python3-dolfin (>= ${fenics:Upstream-Version}),
 dolfin-doc (>= ${fenics:Upstream-Version}),
 dolfin-bin (>= ${fenics:Upstream-Version}),
 python3-ffc (>= ${fenics:Upstream-Version}),
 python3-fiat (>= ${fenics:Upstream-Version}),
 python3-ufl-legacy,
 python-ufl-legacy-doc,
 python3-dijitso (>= ${fenics:Upstream-Version}),
 python3-mshr (>= ${fenics:Upstream-Version}) [!armel !mips !mipsel !alpha !hurd-i386 !ia64 !kfreebsd-amd64 !kfreebsd-i386 !m68k !sh4 !sparc64 !x32],
 ${misc:Depends}
Suggests: python3-mshr (>= ${fenics:Upstream-Version}) [armel mips mipsel alpha hurd-i386 ia64 kfreebsd-amd64 kfreebsd-i386 m68k sh4 sparc64 x32]
Description: Automated Solution of Differential Equations
 FEniCS is a collection of free software for automated, efficient
 solution of differential equations.
 .
 FEniCS has an extensive list of features, including automated
 solution of variational problems, automated error control and
 adaptivity, a comprehensive library of finite elements, high
 performance linear algebra and many more.
 .
 FEniCS is organized as a collection of interoperable components,
 including the problem-solving environment DOLFIN, the form compiler
 FFC, the finite element tabulator FIAT, the just-in-time compiler
 Instant, the code generation interface UFC, the form language UFL and
 a range of additional components.
 .
 This is a metapackage which depends on all FEniCS components.
 .
 This is the legacy version of FEniCS. You may want to consider
 installing the next-generation FEniCS-X (fenicx package).

Package: fenicsx
Architecture: all
Depends: libdolfinx-dev (>= ${fenicsx:Upstream-Epoch-Version}),
 python3-dolfinx (>= ${fenicsx:Upstream-Epoch-Version}),
 dolfinx-doc (>= ${fenicsx:Upstream-Epoch-Version}),
 python3-basix (>= ${fenicsx:Upstream-Version}),
 basix-doc (>= ${fenicsx:Upstream-Version}),
 python3-ffcx (>= ${fenicsx:Upstream-Epoch-Version}),
 python3-ufl (>= ${ufl:Upstream-Version}),
 python-ufl-doc (>= ${ufl:Upstream-Version}),
 ${misc:Depends}
Suggests: python3-dolfinx-mpc (>= ${fenicsx:Upstream-Version}),
 python3-adios4dolfinx  (>= ${fenicsx:Upstream-Version}),
 fenicsx-performance-tests (>= ${fenicsx:Upstream-Version})
Description: Automated Solution of Differential Equations
 FEniCS is a collection of free software for automated, efficient
 solution of differential equations.
 .
 FEniCS has an extensive list of features, including automated
 solution of variational problems, automated error control and
 adaptivity, a comprehensive library of finite elements, high
 performance linear algebra and many more.
 .
 FEniCS is organized as a collection of interoperable components,
 including the problem-solving environment DOLFIN, the form compiler
 FFC, the finite element tabulator FIAT, the just-in-time compiler
 Instant, the code generation interface UFC, the form language UFL and
 a range of additional components.
 .
 This is a metapackage which depends on all FEniCS-X components.
 .
 FEniCS-X is the next-generation implementation of FEniCS
 (with DOLFIN-X, FFC-X)
